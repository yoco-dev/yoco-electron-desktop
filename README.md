# yoco-electron-desktop

## setup  

1. this stuff really only works under windows, so you'll need to run parallels, and use windows as your dev platform  
2. install the [32bit version of nodejs](https://nodejs.org/dist/v6.11.4/node-v6.11.4-x86.msi) in order to build for win32
3. run *npm update* in the root folder to fetch the node modules use by the build infrastructure  

## directory structure  

- build_output *build artefacts*  
-- installer *windows installer packages (full, delta) - produced by electron-winstaller*  
-- package/yoco-win32-ia32 *electron app, built and packaged for win32*  
- crypto *infrastructure for creating and importing a self-signed code-signing cert*  
- electron-source *source code for electron app, excluding config file and binary artefacts*  
- external_resources *icons, config, external DLLs (yoco scala dll)*  
- node_modules *node modules for build infrastructure at directory root*  
- notes *arb notes*  
- YocoDLL *Visual Studio solution for test .DLL*  

## running the raw electron app without packaging

1. change working directory to *electron-source*  
2. run *npm start*  

## build & package  

there are 2 steps:
1. creation of stand-alone electron windows package (rebuilds node modules for target win32 arch, and creates chrome-nodejs standalone executable)  
2. creation of windows installer .exe   

run either *node build.js* or *build.cmd* to launch the combined build & package process  

### build package  

using electron-packager  
package-builder.js  
creates a stand-alone nodejs-chrome application
package is created at build_output/package/yoco-win32-ia32, and will overwrite the last package built    
builds for the specific target architecture, in this case 32-bit windows  
the only thing to note here is that we need to explicitly reference external artefacts, e.g. icons, DLLs  

### build installer

using electron-winstaller  
installer-builder.js  
takes the package built in the previous step, and creates a windows installer (.exe or .msi)
installer will be created at build_output/installer  

### building a new release

if you don't increment the version field in electron-source/package.json, then the installer will just overwrite the last one  
if you DO bump the version number before the build & package, then electron-winstaller will create a new release, numbered after the new version  
two installer packages files will be created, a stand-alone new version, and delta/diff package (not created for first release, obviously), and the RELEASES file will be updated:  

- RELEASES
- yoco-0.0.1-full.nupkg   
- yoco-0.0.2-delta.nupkg  
- yoco-0.0.2-full.nupkg  

contents of RELEASES file:

E8BA7DD53ECDA7D3D1F46B11266C7E30AFDDA385 yoco-0.0.1-full.nupkg 53938823   
88F5B09881FE21EC23F2761917A2F8E3A43D20D8 yoco-0.0.2-delta.nupkg 2231169  
4A0422DA79D61C654CDBAFA0E18A145432111FE3 yoco-0.0.2-full.nupkg 53938179  

the version numbers in the release packages will match the version field in the package.json file  

### auto-update  

the initial install needs to be done manually, but thereafter the app will automatically:  
- check for a new update, at the URL specified in external_resources/config.json (currently localhost:3333)  
- download the new update  
- install and restart the app  

obviously, the release packages need to be made available, so...  
to server up installer release packages from build_output/package

either
*$ http-server ./build_output/installer -a localhost -p 3333*  
or
*serve_installer_packages.cmd*
