param([string]$thumb, [string]$pwd, [string]$outpath)

$certPath = "cert:\localMachine\my\" + $thumb
Write-Host "exporting certificate"
Write-Host "with thumbprint $thumbprint";
Write-Host "from $certPath"

$secure_pwd = ConvertTo-SecureString -String $pwd -Force -AsPlainText

Export-PfxCertificate -cert $certPath -FilePath $outpath -Password $secure_pwd