# run console as admin

New-SelfSignedCertificate -Subject "CN=yoco.co.za" -Type CodeSigningCert -KeySpec "Signature" -KeyUsage "DigitalSignature" -FriendlyName "DEV-yoco-win-code-signing" -NotAfter (get-date).AddYears(1) 

# cert store will default to Local Computer / Personal / Certificates
# -certstorelocation cert:\localmachine\my 

# e.g. thumbprint 86C68B0735EB8FACF81D8808EA4EFA9B1B3410DF CN=yoco.co.za