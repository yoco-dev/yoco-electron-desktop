param([string]$thumb, [string]$outpath)

$certPath = "cert:\localMachine\my\" + $thumb

Write-Host "exporting certificate"
Write-Host "with thumbprint $thumbprint";
Write-Host "from $certPath"

export-Certificate -filepath $outPath -cert $certPath -type CERT  -NoClobber