var packager = require('electron-packager')

var external_resources = [
	'YocoDLL.dll', 
	'Yoco.ico',
	'config.json'
].map(function(fileName, index, array){ return `external_resources/${fileName}` })

var packageOptions = {
	dir: 'electron-source',
	platform: 'win32',
	arch: 'ia32',
	extraResource: external_resources,
	icon: 'external_resources/yoco.ico',
	overwrite: true,
	out: 'build_output/package'
}

function build(next, onFailure) {

	function onPackageCreated(path) {

		console.log('...package built @ ' + path)
		next()
	}

	function onPackageCreationFailed(e) {
		
		console.log('...package build failed:' + e.message)
		onFailure()
	}

	console.log('building package...')

	packager(packageOptions)
		.then(onPackageCreated)
		.catch(onPackageCreationFailed)
}

module.exports.build = build