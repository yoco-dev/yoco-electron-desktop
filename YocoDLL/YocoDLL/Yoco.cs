﻿using System;

namespace YocoDLL
{
    public class Yoco
    {
        private static string _state = "set during static field initialization by DLL";

        public static object SetState(dynamic message)
        {
            _state = (string)message.state;
            return new { state = _state };
        }
        
        public static object GetState()
        {
            return new { state = _state };
        }
    }
}
