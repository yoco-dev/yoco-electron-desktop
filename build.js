var packageBuilder = require('./package-builder.js')
var installerBuilder = require('./installer-builder.js')

function onInstallerBuilt() {
	console.log('build done.')
}

function onInstallerBuildFailed() {
	console.log('building of installer failed, build process terminated.')
}

function onPackageBuilt() {
	console.log('package built')
	installerBuilder.build(onInstallerBuilt, onInstallerBuildFailed)
}

function onPackageBuildFailed() {
	console.log('packaging failed, build process terminated.')
}

packageBuilder.build(onPackageBuilt, onPackageBuildFailed)