var electronInstaller = require('electron-winstaller');

var installerOptions = {

	appDirectory: './build_output/package/yoco-win32-ia32',
	outputDirectory: './build_output/installer',
	authors: 'Yoco Technologies',
	
	exe: 'yoco.exe',
	noMsi: true,
	
	setupExe: 'yoco.exe',
	setupIcon: './external_resources/yoco.ico',
	
	loadingGif: './external_resources/loading.gif',

	certificateFile: './crypto/dev_yoco_win_code_signing.pfx',
	certificatePassword: 'password'
}
// 	setupMsi: 'yoco.msi',

function build(next, onFailure) {

	function onInstallerCreated() {

		console.log('...installer built')

		var blurb = [
			'',
			'on modern windows systems, the installer will install to the following locations:',
			'- c:/Users/???/AppData/local/yoco = binaries and resources',
			'- c:/Users/???/AppData/roaming/yoco = preferences',
			'note that these folders will persist even after yoco is uninstalled'
		]

		blurb.forEach((line) => { console.log(line) })

		next()
	}

	function onInstallerCreationFailed(e) {

		console.log('...installer build failed:')
		console.log(e.message)

		next()
	}

	console.log('building installer...')

	electronInstaller.createWindowsInstaller(installerOptions)
		.then(onInstallerCreated)
		.catch(onInstallerCreationFailed)	
}

module.exports.build = build