const electron = require('electron')
const notRunningPackaged = require('electron-is-dev');
const nconf = require('nconf')
const log4js = require('log4js')
const autoLaunch = require('auto-launch')

// ----------------------------------------------------------------------
// logging

var bootLogger = null;
function configureLogging() {

	log4js.configure({
		appenders: { bootstrap: { type: 'file', filename: 'yoco.bootstrap.log' } },
		categories: { default: { appenders: ['bootstrap'], level: 'info' } }
	})

	bootLogger = log4js.getLogger('bootstrap')
	return bootLogger
}

// ----------------------------------------------------------------------
// ensure singleton app (i.e prevent startup of multiple instances)

function anotherInstanceOfTheAppIsAlreadyRunning(app) {

	function logNotificationFromAdditionalInstanceStartup(commandLine, workingDirectory) {
		bootLogger.info(`detected attempted start-up of second instance. folder ${workingDirectory}, cmdargs ${commandLine}`)
	}

	var alreadyRunning = app.makeSingleInstance(logNotificationFromAdditionalInstanceStartup)

	if (alreadyRunning) {
		bootLogger.info('a primary instance is already running, shutting down this secondary instance')
		app.quit()
		return true
	}

	return false
}

// ----------------------------------------------------------------------
// configure

function configure() {

	// this can't live in the config file, because we need to already know it in order to load the config file
	//
	var resourceRoot = (notRunningPackaged)
		? '../external_resources/'
		: './resources/';
	
	// don't bother loading config from environment vars cuz this is a local desktop app, and not cloud-deployed
	//
	nconf.file({ file: `${resourceRoot}config.json` });

	nconf.set('local:resourceRoot', resourceRoot)
}

// ----------------------------------------------------------------------
// auto-launch

var yocoAutoLauncher = null
function getAutoLauncher() {

	yocoAutoLauncher = (yocoAutoLauncher === null)
		? new autoLaunch({ name: 'Yoco' })
		: yocoAutoLauncher

	return yocoAutoLauncher
}

function checkAndEnableAutoLaunch() {

	var launcher = getAutoLauncher()
	  
	launcher.isEnabled()
		.then(function(isEnabled){
		    if (!isEnabled) { launcher.enable() }
		})
}

function checkAndDisableAutoLaunch() {

	var launcher = getAutoLauncher()

	launcher.isEnabled()
		.then(function(isEnabled){
		    if (isEnabled) { launcher.disable() }
		})
}

// ----------------------------------------------------------------------
// don't run if the squirrel install/update infrastructure has things to do

function restartIsRequired(app){

	if (process.argv.length === 1) {
		bootLogger.info('no (squirrel) args')
		return false;
	}

	const squirrelEvent = process.argv[1];
	bootLogger.info('process.argv[1]:  ' + process.argv[1])
	switch (squirrelEvent) {
		case '--squirrel-install':
		case '--squirrel-updated':
			checkAndEnableAutoLaunch()
			return true
		case '--squirrel-uninstall':
			checkAndDisableAutoLaunch()
			return true
		case '--squirrel-obsolete':
			return true

		case '--squirrel-firstrun':
		default:
			return false
	}
}

// ----------------------------------------------------------------------
// auto-update

function update(restartAfterUpdateDownloaded, beforeRestart) {

	var feedURL = nconf.get('deployment:packageServerURL')
	electron.autoUpdater.setFeedURL(feedURL)

	electron.autoUpdater.addListener("checking-for-update", (event) => {
		bootLogger.info('checking-for-update')
	})

	electron.autoUpdater.addListener("update-available", (event) => {
		bootLogger.info('update-available')
	})

	electron.autoUpdater.addListener("update-not-available", (event) => {
		bootLogger.info('update-not-available')
	})

	electron.autoUpdater.addListener("update-downloaded", (event, releaseNotes, releaseName) => {

		bootLogger.info('update-downloaded')

		if (restartAfterUpdateDownloaded === true) {

			bootLogger.info('restarting app to allow installation of new update')
			beforeRestart()
			setTimeout(function() { electron.autoUpdater.quitAndInstall() }, 500)
		}
	})

	electron.autoUpdater.addListener("error", (error) => {
		bootLogger.info(`error: ${error} ${error.message}`)
	})

	bootLogger.info('checking for updates')
	electron.autoUpdater.checkForUpdates()
}

function handleUpdates(restartAfterUpdateDownloaded, beforeRestart) {

	if (!notRunningPackaged) {

		// this timeout is required because otherwise we sometimes get a filelock error
		//
		setTimeout(function () { update(restartAfterUpdateDownloaded, beforeRestart) }, 500)		
	} 
}

// return false if the app should not start up
//
function startup(app) {

	configureLogging()
	
	if (anotherInstanceOfTheAppIsAlreadyRunning(app)) {
		app.quit()
		return false
	}

	if (restartIsRequired(app)) {

		bootLogger.info('app restart required, restarting...')
		setTimeout(app.quit, 1000)
		return false
	}

	configure()
	return true
}

module.exports.startup = startup
module.exports.handleUpdates = handleUpdates