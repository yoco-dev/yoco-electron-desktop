'use strict'

var {ipcRenderer, remote} = require('electron');  
var main = remote.require("./main.js");

var getStateLabel, getStateButton, setStateInput, setStateButton;

function onGetStateClicked() {

	renderer.getState(function(msg) { getStateLabel.innerHTML = msg.state; });
}

function onSetStateClicked() {

	var state = setStateInput.value;
	var msg = { state: state };
	renderer.setState(msg);
}

function bindDOM() {
 
	getStateLabel = document.getElementById('get-state-label');
	getStateButton = document.getElementById('get-state-button');

	getStateButton.addEventListener('click', onGetStateClicked);

	setStateInput = document.getElementById('set-state-input');
	setStateButton = document.getElementById('set-state-button');
	setStateButton.addEventListener('click', onSetStateClicked);
}

module.exports.getState = main.getState
module.exports.setState = main.setState;
module.exports.init = bindDOM;
