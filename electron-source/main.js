'use strict'

const electron = require('electron')
const path = require('path')
const url = require('url')
const nconf = require('nconf')
var yoco = require('./yoco.js')

const app = electron.app
const ipc = electron.ipcMain
const BrowserWindow = electron.BrowserWindow

var bootStrapper = require('./bootstrapper.js')
if (bootStrapper.startup(app) === false) {
	return
}

var mainWindow = null

function shutDown() {

	yoco.term()
	app.quit()	
}

app.on('window-all-closed', function () {

	// don't shutdown when all windows have been closed, cuz we want to run headless in the system tray

})

app.on('activate', function () {

	/*
	if (mainWindow === null) {
		createWindow()
	}
	*/
})

var tray = null
function initSystemTray() {

	var iconFile = (process.platform === 'darwin')
		? 'yoco_20.png'
		: 'yoco.ico'

	var iconPath = nconf.get('local:resourceRoot') + iconFile

	tray = new electron.Tray(iconPath)

	tray.setToolTip('Yoco Technologies')

	function toggleWindow() {

		if (mainWindow === null) {
			createWindow()
		}
		else {
			mainWindow.close()
		}		
	}

	const contextMenu = electron.Menu.buildFromTemplate([
		{label: 'Toggle Window', click: toggleWindow},
		{label: 'Quit', click: shutDown}
	])

	tray.setContextMenu(contextMenu)
	toggleWindow()
}

function createWindow () {

	mainWindow = new BrowserWindow({width: 320, height: 200})
	mainWindow.on('closed', function () {
		mainWindow = null
	})

	mainWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'index.html'),
		protocol: 'file:',
		slashes: true
	}))

	// Open the DevTools.
	// mainWindow.webContents.openDevTools()
}

function onAppReady() {

	bootStrapper.handleUpdates(true, function() { yoco.term() })
	yoco.init()
	initSystemTray()
}
app.on('ready', onAppReady)

exports.getState = yoco.getState
exports.setState = yoco.setState