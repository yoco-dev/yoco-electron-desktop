'use strict'

// IPC
// http://electron.rocks/different-ways-to-communicate-between-main-and-renderer-process/

function useEdgeFacade() {

	var yes = (process.platform === 'darwin')
	return yes
}

var edgeFacade = {
	func: function(ob) {
		// nop
	}
}

var edge = useEdgeFacade()
	? edgeFacade
	: require('electron-edge')

const nconf = require('nconf')

// you need to change the dll ref in configFunctionSource below by hand
//
const sdkAssembly = 'yoco-desktop-java-lib'
const sdkDLL = sdkAssembly + '.dll'

const configFunctionSource = function() {/*

    async (input) => { 

		// this is required in order to configure the java class loader post ikvm conversion
		// https://sourceforge.net/p/ikvm/wiki/ClassLoader/#runtime-solutions
		// 
		//
		ikvm.runtime.Startup.addBootClassPathAssembly(System.Reflection.Assembly.Load("yoco-desktop-java-lib"));
	
		return "ok";
	}

*/}

var setStateFunctionSource = function() {/*

    async (input) => { 
        //return YocoDLL.Yoco.SetState(input);
        return "something";
    }

*/};

var getStateFunctionSource = function() {/*

    async (input) => { 
        
        //return YocoDLL.Yoco.GetState();

		try { 
			//var x = com.yoco.java.PaymentSdk.instance(); 
			//var x = new com.yoco.java.AdvancedPaymentSdk(null, false);

			var x = new com.yoco.java.AdvancedPaymentSdk(null);

			//var y = YocoDLLInspection.Launcher.Launch();
		} catch (Exception e) {
			Console.WriteLine(e); return "dog";  
		}

        return "success";
    }

*/};


var setStateFn, getStateFn;

function compileEdgeFunctions() {

	const dlls = [

		'IKVM.AWT.WinForms.dll',             
		'IKVM.OpenJDK.Beans.dll',            
		'IKVM.OpenJDK.Charsets.dll',         
		'IKVM.OpenJDK.Cldrdata.dll',         
		'IKVM.OpenJDK.Corba.dll',            
		'IKVM.OpenJDK.Core.dll',             
		'IKVM.OpenJDK.Jdbc.dll',             
		'IKVM.OpenJDK.Localedata.dll',       
		'IKVM.OpenJDK.Management.dll',       
		'IKVM.OpenJDK.Media.dll',            
		'IKVM.OpenJDK.Misc.dll',             
		'IKVM.OpenJDK.Naming.dll',           
		'IKVM.OpenJDK.Nashorn.dll',          
		'IKVM.OpenJDK.Remoting.dll',         
		'IKVM.OpenJDK.Security.dll',         
		'IKVM.OpenJDK.SwingAWT.dll',         
		'IKVM.OpenJDK.Text.dll',             
		'IKVM.OpenJDK.Tools.dll',            
		'IKVM.OpenJDK.Util.dll',             
		'IKVM.OpenJDK.XML.API.dll',          
		'IKVM.OpenJDK.XML.Bind.dll',         
		'IKVM.OpenJDK.XML.Crypto.dll',       
		'IKVM.OpenJDK.XML.Parse.dll',        
		'IKVM.OpenJDK.XML.Transform.dll',    
		'IKVM.OpenJDK.XML.WebServices.dll',  
		'IKVM.OpenJDK.XML.XPath.dll',       
		'IKVM.Runtime.dll',                 
		'IKVM.Runtime.JNI.dll',

		sdkDLL
	]


	var resourceRoot = nconf.get('local:resourceRoot')
	var functionReferences = dlls.map((fileName) => resourceRoot + fileName)

	const configFn = edge.func({
		source:configFunctionSource,
		references:functionReferences
	});

	configFn()

	setStateFn = edge.func({
		source:setStateFunctionSource,
		references:functionReferences
	});

	getStateFn = edge.func({
		source:getStateFunctionSource,
		references:functionReferences
	});
}

function compileEdgeFacades() {

	var state = null;

	setStateFn = function(newState) {
		state = newState;
	}

	getStateFn = function() {
		return state
	}
}

function getState(callBack) {

	getStateFn(null, function (error, result) {
	    if (error) throw error;
	    callBack(result);
	});
}

function setState(msg) {

	setStateFn(msg, function (error, result) {
	    if (error) throw error;
	    else return result;
	});
}

function start() {
	console.log('yoco starting...');

	if (useEdgeFacade() === true) {
		compileEdgeFacades()
	}
	else {
		compileEdgeFunctions()
	}
}

function stop() {
	console.log('...yoco stopped')
}

module.exports.init = start
module.exports.term = stop
module.exports.getState = getState
module.exports.setState = setState